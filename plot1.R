#Read the data
source("readdata.R")

#Plot 1
#Write PNG file
png(file="plot1.png",width = 480, height = 480)

#Make Histogram
hist(data$Global_active_power,col = "red",
     xlab="Global Active Power (kilowatts)", main = "Global Active Power")


#Close file
dev.off()